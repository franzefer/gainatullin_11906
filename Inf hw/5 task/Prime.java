import java.util.Scanner;
public class Prime {
    public static void main (String args[]){
        boolean prime=true;
        int i=2;
        Scanner in=new Scanner(System.in);
        int x=in.nextInt();
        while (i<=(x/2)) {

            if (x%i==0) {
                prime=false;
                break;
            }
            i++;
        }
        if (prime) {
            System.out.print ("prime");
        } else {
            System.out.print ("not prime");
        }
    }
}
