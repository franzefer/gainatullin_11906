/* нумерация алфавита начинается с 0 элемента */
import java.util.Scanner;
public class Chipher {
    public static void main (String args[]) {
        Scanner in = new Scanner(System.in);
        int a = 0;
        int k = in.nextInt(); //ключ отклонения
        while (a != -1) {
            a = in.nextInt();
            if (k>=0) {
                a = a + k;
                if (a < 26) {
                    System.out.println(a);
                } else {
                    System.out.println(a % 26);
                }
            }
            else {
                a=(-a+k)+26;
                if (a < 26) {
                    System.out.println(a);
                } else {
                    System.out.println(a % 26);
                }
            }
        }
    }
}

