import java.util.Scanner;

public class string_calculator {
    private static int Converted(String num) {
        int summ = 0;
        for (int i = 0; i < num.length(); i++) {
            summ = summ * 10 + (int) num.charAt(i) - 48;
        }
        return summ;
    }

    public static void main(String []args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine() + "+";
        String num="";
        int sum=0;
        int sign = 1;
        for (int i = 0; str.length() > i; i++) {
            if (str.charAt(i) == '+' || str.charAt(i) == '-') {
                sum += Converted(num) * sign;
                if (str.charAt(i) == '-') {
                    sign = -1;
                }
                if (str.charAt(i) == '+') {
                    sign = 1;
                }
                num="";
            }
            else {
                num += str.charAt(i);
            }
        }
        System.out.println(sum);
    }
}
