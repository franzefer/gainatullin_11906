import java.util.Scanner;
public class Negativenumm2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] m;
        int check = 0;
        int n = in.nextInt();
        m = new int[n];

        while (n != 0 && check != 1) {
            m[n-1] = in.nextInt();
            if (m[n-1] < 0) {
                check = 1;
            }
            n--;
        }

        if (check == 0) {
            System.out.println("positive");
        }
        if (check == 1) {
            System.out.println("negative");
        }
    }
}

