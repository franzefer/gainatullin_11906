package Arrays;

import java.util.Scanner;

public class Array_sort {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int i = 1, j = 1, tmp = 0, z = 0;
        int n = in.nextInt();
        int m[] = new int[n];

        while (z < n) {
            m[z] = in.nextInt();
            z++;
        }

        while (i < n) {
            int current = m[i];
            j = i - 1;
            while (j >= 0 && current < m[j]) {
                m[j + 1] = m[j];
                j--;
            }
            m[j + 1] = current;
            i++;
        }

        z = 0;
        while (z < n) {
            System.out.println(m[z]);
            z++;
        }
    }
}
