import java.util.Scanner;
public class Negativenum {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int num=1;
        while (n != 0 && num>=0) {
            n--;
            num = in.nextInt();
        }
        if (num<0) {
            System.out.println("negative");
        }
        else {
            System.out.println("positive");
        }
    }
}
